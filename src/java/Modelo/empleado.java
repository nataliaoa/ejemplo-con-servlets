/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author 57311
 */
public class empleado {
    int codigo;
    int cargo;
    String nombre;
    
    
    public empleado(int codigo, int cargo, String nombre, int bono) {
        this.codigo = codigo;
        this.cargo = cargo;
        this.nombre = nombre;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setCargo(int cargo) {
        this.cargo = cargo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public int getCodigo() {
        return codigo;
    }

    public int getCargo() {
        return cargo;
    }

    public String getNombre() {
        return nombre;
    }

public int convertirCargo(String cargoSeleccionado){
    int cargo= Integer.parseInt(cargoSeleccionado);
    return cargo;
}

public static String verCargo(int cargoSeleccionado){
   String cargo="";
   
    if(cargoSeleccionado==0)  cargo="Gerente";
    if(cargoSeleccionado==1)  cargo="Supervisor";
    if(cargoSeleccionado==2)  cargo="Administrador";
    if(cargoSeleccionado==3)  cargo="Limpieza";
    
    return cargo;
}

public static int calcularNomina(String cargo){
        int nomina=0;
        int salarioBase= 1000000;
        if("Gerente".equals(cargo)){
                nomina= salarioBase*4;
            }
            else if("Supervisor".equals(cargo)){
                nomina= salarioBase*3;
            }
            else if("Administrador".equals(cargo)){
                nomina= salarioBase*2;
            }
            else{
                nomina=salarioBase;
            }
        
        
        return nomina;
}
public static int validarBono(String bonoSeleccionado){
    int bono=0;
    if(bonoSeleccionado!=null) bono=500000;
    return bono;
}
}