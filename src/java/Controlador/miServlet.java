/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.empleado;

/**
 *
 * @author 57311
 */
@WebServlet(name = "ServletOperaciones", urlPatterns = {"/ServletOperaciones"})
public class miServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet miServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet miServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>"
            + "<link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css\" integrity=\"sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2\" crossorigin=\"anonymous\">");
            out.println("<title>Nomina</title>");            
            out.println("</head>");
            out.println("<body>");
                   
            String nombreEmpleado= request.getParameter("empleado");
            String codigo= request.getParameter("codigo");
            String cargoSeleccionado= request.getParameter("cargo");
            String bono= request.getParameter("bono");
            String mes= request.getParameter ("mes");
            
            int nomina =0;
            int bonoS=0;
            int total=0;
            String cargo="";
            
            
            cargo= empleado.verCargo(Integer.parseInt(cargoSeleccionado));
            nomina= empleado.calcularNomina(cargo);
            bonoS= empleado.validarBono(bono);
            total= nomina+bonoS;
            out.println("<section class=\"bg-info text-center p-3\">\n" +
"            <h2>NOMINA DE EMPLEADOS</h2>   \n" +
"         \n" +
"        </section>"
                    + "<div class=\"container text-center mt-5\">" 
                    + "<table class=\"table\">\n" +
"  <thead>\n" +
"<tr>\n" +
"          <h4>NOMINA</h4>\n" +
"        </tr>"+
"    <tr>\n" +
"      <th scope=\"col\">Código</th>\n" +
"      <th scope=\"col\">Empleado</th>\n" +
"      <th scope=\"col\">Cargo</th>\n" +
"      <th scope=\"col\">Valor Neto</th>\n" +
"      <th scope=\"col\">Bono</th>\n" + 
"      <th scope=\"col\">Salario a Pagar</th>\n" +
"    </tr>\n" +
"  </thead>\n" +
"  <tbody>\n" +
"    <tr>\n" +
"      <th scope=\"row\">"+codigo+"</th>\n" +
"      <td>"+nombreEmpleado+"</td>\n" +
"      <td>"+cargo+"</td>\n" +
"      <td>"+nomina+"</td>\n" +
"      <td>"+bonoS+"</td>\n" +
"      <td>"+total+"</td>\n" +
"    </tr>\n" +
"  </tbody>\n" +
"</table></div>" 
        + " <div class=\"container text-center mt-5\">"
        + "<h5> La nómina del mes de "+mes+ " para el empleado "+nombreEmpleado+ " con el cargo de "+cargo+ " es de: "+total +"</h5>"
        + "</div>"
        + "<footer class=\"page-footer font-small unique-color-dark bg-info mt-5\" id=\"footer\">\n" +
"      <div class=\"container-expand-md mt-2\" >\n" +
"        <div class=\"container\">\n" +
"\n" +
"\n" +
"          <div class=\"row py-4 d-flex align-items-center\">\n" +
"\n" +
"\n" +
"\n" +
"            <div class=\"col-md-5\">\n" +
"              <ul class=\"text-b\">\n" +
"                <h5 class=\"text-body\">Sitio web creado por:</h5>\n" +
"\n" +
"                <li>Natalia Ortiz Armesto</li>\n" +
"                <li>Duvan Labrador</li>\n" +
"                Programación Web \n" +
"        <a class =\"text-white\" href=\"http://ingsistemas.ufps.edu.co/\" target=\"_blank\">Ing.Sistemas</a> - \n" +
"        <a class =\"text-white\"target=\"_blank\" href=\"http://ufps.edu.co/\">UFPS</a>\n" +
"\n" +
"              </ul>\n" +
"\n" +
"            </div>\n" +
"\n" +
"            <div class=\"col-md-7 text-right mt-2\">\n" +
"\n" +
"            </div>\n" +
"\n" +
"\n" +
"          </div>\n" +
"\n" +
"        </div>\n" +
"\n" +
"\n" +
"      </div>\n" +
"\n" +
"\n" +
"    </footer></body>");
            out.println("</html>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
}
